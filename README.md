# Taurus Anomaly-Detection Application (ASPLOS '22)

In this repository, we share the source code for the anomaly-detection application (AD) presented in our [Taurus: A Data Plane Architecture for Per-Packet ML]() paper (to appear in ASPLOS '22). We also provide details on what is needed to replicate the end-to-end testbed used for evaluating the AD application.

## Description
The anomaly-detection application is designed to identify malicious packets by drawing (a) similarities to known attack signatures and (b) distinctions to typical benign traffic. Our particular implementation uses machine-learning techniques to construct a dense, deep neural network (DNN), similar to the one described [here](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7777224&tag=1). We then train the model on synthetic data generated from the [NSL-KDD Intrusion Detection dataset](https://www.unb.ca/cic/datasets/nsl.html).

Using our testbed platform, described below, we send both malicious and benign traffic from one server (i.e., the sender) and receive it on another server (i.e., the receiver) for post-analysis; we use [MoonGen](https://github.com/emmericp/MoonGen) for sending and receiving traffic. In between, the traffic passes through the P4-programmable Tofino switch (implementing Taurus pre- and post-processing MAT blocks) and the Xilinx FPGA (implementing the Taurus MapReduce block). Upon receiving a packet from the sender, the switch performs the following steps: (1) extracts and prepocesses the features (e.g., host-to-network order conversion to/from the FPGA and fixed-point conversion) and forwards packets to the FPGA, and (2) postprocesses and routes the packets coming from the FPGA to the receiver. The FPGA implements the DNN, written in [Spatial](https://spatial-lang.org/), and tags the packets with a prediction value (i.e., malicious or benign).

In our AD usecase, we use 64 byte packets. We create a new header---consisting of machine-learning features (encoded as an array of seven 32-bit elements), one-byte ground truth label, and a one-byte prediction value---and encapsulate it inside the IP header (as shown in the packet format, below). Each feature element is a fixed-point value with 16 integer bits and 16 decimal bits. 

```
|------------------------- Packet Header Format ---------------------------|
|   14 B   | 20 B |         28 B         |        1 B         |     1 B    |
|--------------------------------------------------------------------------|
| Ethernet |  IP  | 7 x 32-bit ML Fields | Ground Truth Label | Prediction |
|--------------------------------------------------------------------------|
```

Lastly, the receiver tabulates the results and uses the ground truth label along with the prediction value to calculate an F1 score. In our Taurus paper, we receive an F1 score of 71.1.

## Directory Structure

Our application is composed of multiple programmable components (e.g., [ONOS](https://opennetworking.org/onos/), [P4/Tofino](https://p4.org/), [Spatial/MapReduce](), Python), operating together as shown in the platform design, below. The source code for the application is available under the `src` folder:

```sh
src
 |--app      # High-level python script to install flow rules using ONOS' Rest API
 |--onos     # ONOS application and pipeline code for interfacing with the Tofino switch
 |--p4       # P4 code that runs on the Tofino switch, implementing the Pre-/Pre-processing Taurus blocks
 |--spatial  # Spatial code that implements the Taurus MapReduce block, running the AD neural network
```

## Platform Design and Dependencies

When running the anomaly-detection application using our Taurus switch architecture, we expect a testbed as shown in the figure below. (For a high-level overview, please read the end-to-end evalaution section of our [ASPLOS '22 paper]().)

<img src="doc/platform.png" alt="Platform Design" width="600">

It consists of the following industry-standard and open-source tools:

- **[ONOS: Open Network Operating System](https://opennetworking.org/onos/)**
  - We use the docker version 2.2.2, publicly available on Docker Hub, [here](https://hub.docker.com/r/onosproject/onos/). 
- **[EdgeCore Wedge 100BF-32X P4-Programmable Tofino Switch](https://www.edge-core.com/productsInfo.php?cls=1&cls2=5&cls3=181&id=335)** running [Stratum](https://opennetworking.org/stratum/) with [Open Network Linux (ONL)](http://opennetlinux.org/).
  - We use ONL version 4.14. For Stratum, we built a custom docker version against Barefoot SDE 9.2.0, using the instructions provided in the Stratum GitHub repository, [here](https://github.com/stratum/stratum/tree/main/stratum/hal/bin/barefoot/docker)
    - **Note:** You can use existing public Stratum OS docker images as well, matching the ONL version running on your switch, https://registry.hub.docker.com/r/stratumproject/stratum-bf.
- **[Open-Source Taurus MapReduce Architecture](https://gitlab.com/dataplane-ai/taurus/mapreduce)** based on Stanford [Spatial DSL](https://github.com/stanford-ppl/spatial) and Xilinx [Open-NIC Shell](https://github.com/Xilinx/open-nic-shell) with their [Alveo U250 FPGA board](https://www.xilinx.com/products/boards-and-kits/alveo.html).
  - We have made the MapReduce architecture available publicly, here: https://gitlab.com/dataplane-ai/taurus/mapreduce.
  - We use the following commits for Spatial DSL and Open-NIC Shell repositorires.
    - Spatial DSL: commit [`36fb4616563`](https://github.com/stanford-ppl/spatial/tree/36fb461656312265416113c4dbb8cc51faaf8176) 
      - **Note:** We added custom support for the AXI Stream Protocol for routing packets through the Spatial DSL.
    - Open-NIC Shell: commit [`7daf2422ed`](https://github.com/Xilinx/open-nic-shell/tree/7daf2422ed05b410abce6a98c3f01811efd04af4)
- **[MoonGen Traffic Generator](https://github.com/emmericp/MoonGen)** for sending and receiving packets.

## Contact Us 
- [Tushar Swamy](https://www.linkedin.com/in/tushar-swamy-b4aa51b1/)
- [Alexander Rucker](https://www.linkedin.com/in/acrucker/)
- [Annus Zulfiqar](https://annusgit.github.io/)
- [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
