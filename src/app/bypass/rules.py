#!/usr/bin/env python3

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import rest
import argparse
import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Bypass Script')
    parser.add_argument('--controller', help='Controller IP',
                        type=str, action="store", default='localhost')
    parser.add_argument('--switch-type', help='Switch type (bmv2 or tofino)',
                        type=str, action="store", default='bmv2')
    parser.add_argument('--clear', action="store_true", default=False)
    parser.add_argument('--hosts', help='Hosts',
                        type=str, action="store", required=True, default='')
    parser.add_argument('--fxpt', action="store_true", default=True)
    parser.add_argument('--fpga', action="store_true", default=False)
    parser.add_argument('--external-ports', help='FPGA ports',
                        type=str, action="store", default='')
    args = parser.parse_args()

    rest.set_device_id(args.switch_type)
    rest.set_restful_getpost_url(args.controller)
    rest.set_debug_mode(True)

    with open(args.hosts) as file:
        hosts = json.load(file)['hosts']

    edo3 = hosts['edo3']
    edo3_intf0 = hosts['edo3']['interfaces'][0]
    edo3_intf0_port = edo3_intf0['port']
    edo3_intf0_ip = edo3_intf0['ip'][:edo3_intf0['ip'].find('/')]
    edo3_intf0_mac = edo3_intf0['mac']
    edo3_intf1 = hosts['edo3']['interfaces'][1]
    edo3_intf1_port = edo3_intf1['port']
    edo3_intf1_ip = edo3_intf1['ip'][:edo3_intf1['ip'].find('/')]
    edo3_intf1_mac = edo3_intf1['mac']

    edo4 = hosts['edo4']
    edo4_intf0 = hosts['edo4']['interfaces'][0]
    edo4_intf0_port = edo4_intf0['port']
    edo4_intf0_ip = edo4_intf0['ip'][:edo4_intf0['ip'].find('/')]
    edo4_intf0_mac = edo4_intf0['mac']
    edo4_intf1 = hosts['edo4']['interfaces'][1]
    edo4_intf1_port = edo4_intf1['port']
    edo4_intf1_ip = edo4_intf1['ip'][:edo4_intf1['ip'].find('/')]
    edo4_intf1_mac = edo4_intf1['mac']

    external_interfaces = json.loads(args.external_ports)
    fpga_port = external_interfaces['fpga']

    def clear():
        rest.delete_rule(table="forward", ingress_port=edo3_intf0_port)
        rest.delete_rule(table="forward", ingress_port=edo4_intf0_port)
        rest.delete_rule(table="forward", ingress_port=fpga_port, eth_src=edo3_intf0_mac)
        rest.delete_rule(table="forward", ingress_port=fpga_port, eth_src=edo4_intf0_mac)

    if args.clear:
        clear()
    else:
        if not args.fpga:
            rest.install_rule(table="forward", ingress_port=edo3_intf0_port,
                              output=edo4_intf0_port, is_permanent=True)
            rest.install_rule(table="forward", ingress_port=edo4_intf0_port,
                              output=edo3_intf0_port, is_permanent=True)
        else:
            rest.install_rule(table="forward", ingress_port=edo3_intf0_port,
                              output=fpga_port, is_permanent=True)
            rest.install_rule(table="forward", ingress_port=edo4_intf0_port,
                              output=fpga_port, is_permanent=True)
            rest.install_rule(table="forward", ingress_port=fpga_port, eth_src=edo3_intf0_mac, 
                              output=edo4_intf0_port, is_permanent=True)
            rest.install_rule(table="forward", ingress_port=fpga_port, eth_src=edo4_intf0_mac, 
                              output=edo3_intf0_port, is_permanent=True)
            if args.fxpt:
                rest.install_rule(table="format_fxpt", egress_port=fpga_port, is_permanent="true")
