#!/usr/bin/env python3

# *************************************************************************
#
# Copyright 2021 Tushar Swamy (Stanford University),
#                Alexander Rucker (Stanford University),
#                Annus Zulfiqar (Purdue University),
#                Muhammad Shahbaz (Stanford/Purdue University)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# *************************************************************************

import binascii
import json
import socket

import requests


AUTH = ('onos', 'rocks')

DEVICE_ID = "device:s1-"
DEFAULT_PRIORITY = 400
DEFAULT_TABLE = "IngressPipeImpl.forward_control.forward_table"
DEFAULT_INGRESS_MASK = 255

RESTFUL_CONTROLLER_IP = "edo2.stanford.edu"
RESTFUL_BASE_URL = "http://{0}:8181/onos/v1".format(RESTFUL_CONTROLLER_IP)
RESTFUL_GETPOST_URL = RESTFUL_BASE_URL + "/flows"

DEBUG_MODE = False


def set_restful_getpost_url(controller_ip):
    global RESTFUL_BASE_URL
    global RESTFUL_GETPOST_URL
    RESTFUL_BASE_URL = "http://{0}:8181/onos/v1".format(controller_ip)
    RESTFUL_GETPOST_URL = RESTFUL_BASE_URL + "/flows"


def set_device_id(id):
    global DEVICE_ID
    DEVICE_ID += id


def set_debug_mode(mode):
    global DEBUG_MODE
    DEBUG_MODE = mode


def install_rule(table=DEFAULT_TABLE,
                 priority=DEFAULT_PRIORITY,
                 ingress_port=None,
                 ingress_port_mask=DEFAULT_INGRESS_MASK,
                 egress_port=None,
                 eth_dst=None,
                 eth_src=None,
                 eth_type=None,
                 ipv4_src=None,
                 ipv4_dst=None,
                 ipv4_proto=None,
                 output=None,
                 int=False,
                 int_eth_src=None,
                 int_eth_dst=None,
                 int_ipv4_src=None,
                 int_ipv4_dst=None,
                 int_udp_dst=None,
                 is_permanent=False):
    TABLE_ID = DEFAULT_TABLE
    if table == "forward":
        TABLE_ID = "IngressPipeImpl.forward_control.forward_table"
    elif table == "int_ingress":
        TABLE_ID = "IngressPipeImpl.int_ingress.int_ingress_table"
    elif table == "int_egress":
        TABLE_ID = "EgressPipeImpl.int_egress.int_egress_table"
    elif table == "format_fxpt":
        TABLE_ID = "EgressPipeImpl.format_fxpt_control.format_table"
    elif table == "accumulate":
        TABLE_ID = "IngressPipeImpl.accumulate_control.accumulate_table"
    elif table == "flow_idx":
        TABLE_ID = "IngressPipeImpl.flow_idx_control.flow_idx_table"
    elif table == "block":
        TABLE_ID = "IngressPipeImpl.block_control.block_table"
    elif table == "traffic":
        TABLE_ID = "IngressPipeImpl.anomaly_control.anomaly_table"

    PRIORITY = priority

    matches_list = []
    if ingress_port != None:
        matches_list += [{
            "field": "standard_metadata.ingress_port",
            "match": "ternary",
            "value": "{:02x}".format(ingress_port),
            "mask": "{:02x}".format(ingress_port_mask)
        }]
    if egress_port:
        matches_list += [{
            "field": "standard_metadata.egress_port",
            "match": "ternary",
            "value": "{:02x}".format(egress_port),
            "mask": "ff"
        }]
    if eth_dst:
        matches_list += [{
            "field": "hdr.ethernet.dst_addr",
            "match": "ternary",
            "value": eth_dst.translate({ord(':'): None}).lower(),
            "mask": "ffffffffffff"
        }]
    if eth_src:
        matches_list += [{
            "field": "hdr.ethernet.src_addr",
            "match": "ternary",
            "value": eth_src.translate({ord(':'): None}).lower(),
            "mask": "ffffffffffff"
        }]
    if eth_type:
        matches_list += [{
            "field": "hdr.ethernet.ether_type",
            "match": "ternary",
            "value": "{:04x}".format(eth_type),
            "mask": "ffff"
        }]
    if ipv4_dst:
        matches_list += [{
            "field": "hdr.ipv4.dst_addr",
            "match": "ternary",
            "value": binascii.b2a_hex(socket.inet_aton(ipv4_dst)).decode("ascii"),
            "mask": "ffffffff"
        }]
    if ipv4_src:
        matches_list += [{
            "field": "hdr.ipv4.src_addr",
            "match": "ternary",
            "value": binascii.b2a_hex(socket.inet_aton(ipv4_src)).decode("ascii"),
            "mask": "ffffffff"
        }]
    if ipv4_proto:
        matches_list += [{
            "field": "local_metadata.ip_proto",
            "match": "ternary",
            "value": "{:02x}".format(ipv4_proto),
            "mask": "ff"
        }]

    instructions_list = []

    if table == "forward":
        if output:
            instructions_list += [{
                "type": "PROTOCOL_INDEPENDENT",
                "subtype": "ACTION",
                "actionId": "IngressPipeImpl.forward_control.set_output_port",
                "actionParams": {
                    "port_num": format(output, 'x')
                }
            }]
        else:
            instructions_list += [{
                "type": "PROTOCOL_INDEPENDENT",
                "subtype": "ACTION",
                "actionId": "IngressPipeImpl.forward_control.drop",
                "actionParams": {}
            }]
    elif table == "int_ingress":
        if int:
            instructions_list += [{
                "type": "PROTOCOL_INDEPENDENT",
                "subtype": "ACTION",
                "actionId": "IngressPipeImpl.int_ingress.clone_int_packet",
                "actionParams": {
                    "switch_id": "{:04x}".format(0)
                }
            }]
        else:
            instructions_list += [{
                "type": "PROTOCOL_INDEPENDENT",
                "subtype": "ACTION",
                "actionId": "IngressPipeImpl.int_ingress.drop",
                "actionParams": {}
            }]
    elif table == "int_egress":
        if int:
            instructions_list += [{
                "type": "PROTOCOL_INDEPENDENT",
                "subtype": "ACTION",
                "actionId": "EgressPipeImpl.int_egress.gen_int_report",
                "actionParams": {
                    "src_mac": int_eth_src.translate({ord(':'): None}).lower(),
                    "int_mac": int_eth_dst.translate({ord(':'): None}).lower(),
                    "src_ip": binascii.b2a_hex(socket.inet_aton(int_ipv4_src)).decode("ascii"),
                    "int_ip": binascii.b2a_hex(socket.inet_aton(int_ipv4_dst)).decode("ascii"),
                    "int_port": "{:04x}".format(int_udp_dst)
                }
            }]
    elif table == "format_fxpt":
        instructions_list += [{
            "type": "PROTOCOL_INDEPENDENT",
            "subtype": "ACTION",
            "actionId": "EgressPipeImpl.format_fxpt_control.shift_field",
            "actionParams": {}
        }]
    elif table == "block":
        instructions_list += [{
            "type": "PROTOCOL_INDEPENDENT",
            "subtype": "ACTION",
            "actionId": "IngressPipeImpl.block_control.pass",
            "actionParams": {}
        }]
    elif table == "accumulate":
        instructions_list += [{
            "type": "PROTOCOL_INDEPENDENT",
            "subtype": "ACTION",
            "actionId": "IngressPipeImpl.accumulate_control.accumulate_field",
            "actionParams": {}
        }]
    elif table == "flow_idx":
        instructions_list += [{
            "type": "PROTOCOL_INDEPENDENT",
            "subtype": "ACTION",
            "actionId": "IngressPipeImpl.flow_idx_control.get_flow_idx",
            "actionParams": {}
        }]
    elif table == "traffic":
        instructions_list += [{
            "type": "PROTOCOL_INDEPENDENT",
            "subtype": "ACTION",
            "actionId": "IngressPipeImpl.anomaly_control.mark_anomaly",
            "actionParams": {}
        }]
    else:
        pass  # install NoAction

    if matches_list:
        json_data = {
            "priority": PRIORITY,
            "timeout": 0,
            "isPermanent": is_permanent,
            "deviceId": DEVICE_ID,
            "tableId": TABLE_ID,
            "treatment": {
                "instructions": instructions_list
            },
            "selector": {
                "criteria": [
                    {
                        "type": "PROTOCOL_INDEPENDENT",
                        "matches": matches_list
                    }
                ]
            }
        }
    else:
        json_data = {
            "priority": PRIORITY,
            "timeout": 0,
            "isPermanent": is_permanent,  # "true"
            "deviceId": DEVICE_ID,
            "tableId": TABLE_ID,
            "treatment": {
                "instructions": instructions_list
            }
        }

    response = requests.post(RESTFUL_GETPOST_URL + "/" + DEVICE_ID, data=json.dumps(json_data), auth=AUTH)

    if DEBUG_MODE:
        print(json_data)
        print("Response is: {0}".format(response))


# def wrapped_install_rule(args):
#     install_rule(table=args['table'], ipv4_src=args['ipv4_src'], priority=args['priority'])
#
#
# # TODO: make it general
# def install_rule_parallel(rule_list):
#     pool = Pool(cpu_count() - 1)
#     pool.map(wrapped_install_rule, rule_list)


def get_flow_id(table=DEFAULT_TABLE,
                ingress_port=None,
                egress_port=None,
                eth_dst=None,
                eth_src=None,
                eth_type=None,
                ipv4_src=None,
                ipv4_dst=None,
                ipv4_proto=None):
    response = requests.get(RESTFUL_GETPOST_URL, auth=AUTH)
    response_json = response.json()
    flows = response_json["flows"]

    TABLE_ID = DEFAULT_TABLE
    if table == "forward":
        TABLE_ID = "IngressPipeImpl.forward_control.forward_table"
    elif table == "int_ingress":
        TABLE_ID = "IngressPipeImpl.int_ingress.int_ingress_table"
    elif table == "int_egress":
        TABLE_ID = "EgressPipeImpl.int_egress.int_egress_table"
    elif table == "format_fxpt":
        TABLE_ID = "EgressPipeImpl.format_fxpt_control.format_table"
    elif table == "accumulate":
        TABLE_ID = "IngressPipeImpl.accumulate_control.accumulate_table"
    elif table == "flow_idx":
        TABLE_ID = "IngressPipeImpl.flow_idx_control.flow_idx_table"
    elif table == "block":
        TABLE_ID = "IngressPipeImpl.block_control.block_table"
    elif table == "traffic":
        TABLE_ID = "IngressPipeImpl.anomaly_control.anomaly_table"

    matches_dict = dict()
    if ingress_port:
        matches_dict["standard_metadata.ingress_port"] = "{:02x}".format(ingress_port)
    if egress_port:
        matches_dict["standard_metadata.egress_port"] = "{:02x}".format(egress_port)
    if eth_dst:
        matches_dict["hdr.ethernet.dst_addr"] = eth_dst.translate({ord(':'): None}).lower()
    if eth_src:
        matches_dict["hdr.ethernet.src_addr"] = eth_src.translate({ord(':'): None}).lower()
    if eth_type:
        matches_dict["hdr.ethernet.ether_type"] = "{:04x}".format(eth_type)
    if ipv4_dst:
        matches_dict["hdr.ipv4.dst_addr"] = binascii.b2a_hex(socket.inet_aton(ipv4_dst)).decode("ascii")
    if ipv4_src:
        matches_dict["hdr.ipv4.src_addr"] = binascii.b2a_hex(socket.inet_aton(ipv4_src)).decode("ascii")
    if ipv4_proto:
        matches_dict["local_metadata.ip_proto"] = "{:02x}".format(ipv4_proto)

    # TODO: refine this further
    for flow in flows:
        if flow["tableId"] == TABLE_ID:
            for criteria in flow["selector"]["criteria"]:
                if criteria["type"] == "PROTOCOL_INDEPENDENT":
                    matches = criteria["matches"]
                    if len(matches) == len(matches_dict):
                        is_matched = True
                        for match in matches:
                            if matches_dict[match["field"]] != match["value"]:
                                is_matched = False
                        if is_matched:
                            return flow["id"]

    return None


def delete_rule(table=DEFAULT_TABLE,
                ingress_port=None,
                egress_port=None,
                eth_dst=None,
                eth_src=None,
                eth_type=None,
                ipv4_src=None,
                ipv4_dst=None,
                ipv4_proto=None):
    flow_id = get_flow_id(table, ingress_port, egress_port, eth_dst, eth_src, eth_type, ipv4_src, ipv4_dst, ipv4_proto)
    if flow_id:
        response = requests.delete(RESTFUL_GETPOST_URL + "/" + DEVICE_ID + "/" + flow_id, auth=AUTH)

        if DEBUG_MODE:
            print("Response is: {0}".format(response))
