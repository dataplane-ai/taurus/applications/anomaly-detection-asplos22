/*
 * Copyright 2021 Tushar Swamy (Stanford University),
 *                Alexander Rucker (Stanford University),
 *                Annus Zulfiqar (Purdue University),
 *                Muhammad Shahbaz (Stanford/Purdue University)
 *
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.taurus.pipelines.anomaly_detection;

import com.google.common.collect.ImmutableList;
import org.onosproject.core.CoreService;
import org.onosproject.net.behaviour.Pipeliner;
import org.onosproject.net.driver.DriverAdminService;
import org.onosproject.net.driver.DriverProvider;
import org.onosproject.net.pi.model.DefaultPiPipeconf;
import org.onosproject.net.pi.model.PiPipeconf;
import org.onosproject.net.pi.model.PiPipeconfId;
import org.onosproject.net.pi.model.PiPipelineInterpreter;
import org.onosproject.net.pi.model.PiPipelineModel;
import org.onosproject.net.pi.service.PiPipeconfService;
import org.onosproject.p4runtime.model.P4InfoParser;
import org.onosproject.p4runtime.model.P4InfoParserException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.onosproject.net.pi.model.PiPipeconf.ExtensionType.*;

/**
 * Component that produces and registers the basic pipeconfs when loaded.
 */
@Component(immediate = true)
public final class PipeconfLoader {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    private PiPipeconfService piPipeconfService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    private CoreService coreService;

    @Reference(cardinality = ReferenceCardinality.MANDATORY)
    private DriverAdminService driverAdminService;

    @Activate
    public void activate() {
        coreService.registerApplication(Constants.APP_NAME);
        // Registers all pipeconf at component activation.
        if (piPipeconfService.getPipeconf(Constants.TOFINO_PIPECONF_ID).isPresent()) {
            piPipeconfService.unregister(Constants.TOFINO_PIPECONF_ID);
        }
        removePipeconfDrivers(Constants.TOFINO_PIPECONF_ID);

        piPipeconfService.register(buildTofinoPipeconf());
    }

    @Deactivate
    public void deactivate() {
        if (piPipeconfService.getPipeconf(Constants.TOFINO_PIPECONF_ID).isPresent()) {
            piPipeconfService.unregister(Constants.TOFINO_PIPECONF_ID);
        }
        removePipeconfDrivers(Constants.TOFINO_PIPECONF_ID);
    }

    private static PiPipeconf buildTofinoPipeconf() {
        final URL p4InfoUrl = PipeconfLoader.class.getResource(Constants.TOFINO_P4INFO_PATH);
        final PiPipelineModel pipelineModel = parseP4Info(p4InfoUrl);

        return DefaultPiPipeconf.builder()
                .withId(Constants.TOFINO_PIPECONF_ID)
                .withPipelineModel(pipelineModel)
                .addBehaviour(PiPipelineInterpreter.class, InterpreterImpl.class)
                .addBehaviour(Pipeliner.class, PipelinerImpl.class)
                .addExtension(P4_INFO_TEXT, p4InfoUrl)
                .addExtension(TOFINO_BIN,
                        PipeconfLoader.class.getResource(Constants.TOFINO_BIN_PATH))
                .addExtension(TOFINO_CONTEXT_JSON,
                        PipeconfLoader.class.getResource(Constants.TOFINO_CTX_PATH))
                .addExtension(CPU_PORT_TXT,
                        PipeconfLoader.class.getResource(Constants.TOFINO_CPU_PORT_PATH))
                .build();
    }

    private static PiPipelineModel parseP4Info(URL p4InfoUrl) {
        try {
            return P4InfoParser.parse(p4InfoUrl);
        } catch (P4InfoParserException e) {
            throw new IllegalStateException(e);
        }
    }

    private void removePipeconfDrivers(PiPipeconfId piPipeconfId) {
        List<DriverProvider> driverProvidersToRemove = driverAdminService
                .getProviders().stream()
                .filter(p -> p.getDrivers().stream()
                        .anyMatch(d -> d.name().endsWith(piPipeconfId.id())))
                .collect(Collectors.toList());

        if (driverProvidersToRemove.isEmpty()) {
            return;
        }

        log.info("Found {} outdated drivers for pipeconf '{}', removing...",
                driverProvidersToRemove.size(), piPipeconfId);

        driverProvidersToRemove.forEach(driverAdminService::unregisterProvider);
    }
}
