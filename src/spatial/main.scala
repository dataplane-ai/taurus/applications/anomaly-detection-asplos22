// *************************************************************************
//
// Copyright 2021 Tushar Swamy (Stanford University),
//                Alexander Rucker (Stanford University),
//                Annus Zulfiqar (Purdue University),
//                Muhammad Shahbaz (Stanford/Purdue University)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// *************************************************************************

package spatial.tests.feature.transfers

import argon.static.Sym
import spatial.dsl._

@spatial class MapReduce extends SpatialTest {
  import spatial.lang.{AxiStream512, AxiStream512Bus}
  // @struct case class AxiStream512(tdata: U512, tstrb: U64, tkeep: U64, tlast: Bit, tid: U8, tdest: U8, tuser: U64)
  def main(args: Array[String]): Unit = {
    // In/out buses here have type AxiStream512, meaning you can access all the axis fields in the Spatial source code (tdata, tstrb, tkeep, tlast, tid, tdest, tuser)
    //  If you only care about the tdata field, you should use type U512 instead of AxiStream512
 
    
    type T = FixPt[TRUE, _16, _16] 
    //type T = Float

    val inbus = StreamIn[AxiStream512](AxiStream512Bus(tid = 0, tdest = 0))
    val outbus = StreamOut[AxiStream512](AxiStream512Bus(tid = 0, tdest = 1))
    
    val project_dir = sys.env("PLATFORM_ROOT_DIR") + s"/project/anomaly-detection/src/spatial/luts/"
    
    Accel {

      val input = SRAM[T](7)
      val packet_use1 = FIFO[AxiStream512](2)
      val packet_use2 = FIFO[AxiStream512](10)
      val dummy_input_fifo = FIFO[T](2)
      val dummy_stage0_fifo = FIFO[T](2)
      val dummy_stage1_fifo = FIFO[T](2)
      val dummy_stage2_fifo = FIFO[T](2)
      val dummy_stage3_fifo = FIFO[T](2)


    
      //val L0_res = SRAM[T](1)
      //val L0_W_LUT = LUT.fromFile[T](1,7)(project_dir + "TEST_W_LUT.csv")
      //val L0_B_LUT = LUT.fromFile[T](12)(project_dir + "TEST_B_LUT.csv")
      
      val L0_res = SRAM[T](12)
      val L1_res = SRAM[T](6)
      val L2_res = SRAM[T](3)
      val L3_res = SRAM[T](2)

      val L0_W_LUT = LUT.fromFile[T](12,7)(project_dir + "L0_NEURON_W_LUT.csv")
      val L0_B_LUT = LUT.fromFile[T](12)(project_dir + "L0_NEURON_B_LUT.csv")
      val L1_W_LUT = LUT.fromFile[T](6,12)(project_dir + "L1_NEURON_W_LUT.csv")
      val L1_B_LUT = LUT.fromFile[T](6)(project_dir + "L1_NEURON_B_LUT.csv")
      val L2_W_LUT = LUT.fromFile[T](3,6)(project_dir + "L2_NEURON_W_LUT.csv")
      val L2_B_LUT = LUT.fromFile[T](3)(project_dir + "L2_NEURON_B_LUT.csv")
      val L3_W_LUT = LUT.fromFile[T](2,3)(project_dir + "L3_NEURON_W_LUT.csv")
      val L3_B_LUT = LUT.fromFile[T](2)(project_dir + "L3_NEURON_B_LUT.csv")




      Stream.Foreach(*) { stream_idx =>

        Pipe {
          val packet = inbus.value
          packet_use1.enq(packet)
          packet_use2.enq(packet)
        }


        Pipe {
          val packet = packet_use1.deq()
          val eth = 112
          val ip = 160
          val shift_amounts = Seq.tabulate(7){i => (eth + ip + (i * 32)).to[I16]}
          Foreach(0 until 7 par 7){ i =>
              val mux1H_conds = Seq.tabulate(7){j => j.to[I32] === i}
              val shifted_pkt = oneHotMux(mux1H_conds, shift_amounts.map{amt => packet.tdata.as[U512] >> amt})
              input(i) = cat(shifted_pkt.bits(7::0), shifted_pkt.bits(15::8), shifted_pkt.bits(23::16), shifted_pkt.bits(31::24)).as[T]
          }
          dummy_input_fifo.enq(input(6))
        }
        
        /*
        Pipe {
          val packet = packet_use1.deq()
          Foreach(0 until 7 par 7){ i =>
            val eth = 112
            val ip = 160
            val shift = eth + ip + ((i)*32)
            val shifted_pkt = (packet.tdata.as[U512] >> shift.as[I16])
            input(i) = cat(shifted_pkt.bits(7::0), shifted_pkt.bits(15::8), shifted_pkt.bits(23::16), shifted_pkt.bits(31::24)).as[T]

          }
          dummy_input_fifo.enq(input(6))

        }
        */
  
        
        Pipe {
         
         val dummy = dummy_input_fifo.deq()
          
         List.tabulate(12) { i =>

              val partial_results = List.tabulate(7) { j =>
                  L0_W_LUT(i, j) * input(j)
              }

              val w = partial_results.reduceTree {_+_}
              L0_res(i) = max(w + L0_B_LUT(i), 0)

          }

          dummy_stage0_fifo.enq(L0_res(11))
        }
      
        Pipe {
          
          val dummy = dummy_stage0_fifo.deq()
        
          List.tabulate(6) { i =>
  
              val partial_results = List.tabulate(12) { j =>
                  L1_W_LUT(i, j) * L0_res(j)
              }

              val w = partial_results.reduceTree {_+_}
              L1_res(i) = max(w + L1_B_LUT(i), 0)

          }
          
          dummy_stage1_fifo.enq(L1_res(5))
        }
        

        Pipe {
          val dummy = dummy_stage1_fifo.deq()
        
          List.tabulate(3) { i =>

              val partial_results = List.tabulate(6) { j =>
                  L2_W_LUT(i, j) * L1_res(j)
              }

              val w = partial_results.reduceTree {_+_}
              L2_res(i) = max(w + L2_B_LUT(i), 0)
        
          }
          
          dummy_stage2_fifo.enq(L2_res(2))
        }


        Pipe {
          val dummy = dummy_stage2_fifo.deq()
        
          List.tabulate(2) { i =>

              val partial_results = List.tabulate(3) { j =>
                  L3_W_LUT(i, j) * L2_res(j)
              }

              val w = partial_results.reduceTree {_+_}
              L3_res(i) = max(w + L3_B_LUT(i), 0)
        
          }
          dummy_stage3_fifo.enq(L3_res(1))
        }

                
                
        Pipe {
         val dummy = dummy_stage3_fifo.deq()
         //val dummy = dummy_stage1_fifo.deq()
         //val dummy = dummy_input_fifo.deq()
  
         val decision = mux(L3_res(0) >= L3_res(1), 0, 1)
         //val decision = mux(input(0) >= input(1), 1, 2)

         val packet = packet_use2.deq()
         val newPacket = AxiStream512((packet.tdata.as[U512]) | (decision.as[U512] << 504), packet.tstrb, packet.tkeep, packet.tlast, packet.tid, 1, 0)
         //val newPacket = AxiStream512((packet.tdata.as[U512]) | (L1_res(1).as[U512] << 480), packet.tstrb, packet.tkeep, packet.tlast, packet.tid, 1, 0)
         outbus := newPacket
        }

      }

    }

    assert(1 == 1) // Assert keeps spatial happy
  }
}
