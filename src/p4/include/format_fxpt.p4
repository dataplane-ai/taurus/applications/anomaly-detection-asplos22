/*
 * Copyright 2021 Tushar Swamy (Stanford University),
 *                Alexander Rucker (Stanford University),
 *                Annus Zulfiqar (Purdue University),
 *                Muhammad Shahbaz (Stanford/Purdue University)
 *
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FORMAT_FXPT__
#define __FORMAT_FXPT__

#include "headers.p4"
#include "defines.p4"

control format_fxpt_control(inout parsed_headers_t hdr,
                        inout standard_metadata_t standard_metadata) {


    action shift_field() {
        hdr.ml_fields.field0 = hdr.ml_fields.field0 << 16;
        hdr.ml_fields.field1 = hdr.ml_fields.field1 << 16;
        hdr.ml_fields.field2 = hdr.ml_fields.field2 << 16;
        hdr.ml_fields.field3 = hdr.ml_fields.field3 << 16;
        hdr.ml_fields.field4 = hdr.ml_fields.field4 << 16;
        hdr.ml_fields.field5 = hdr.ml_fields.field5 << 16;
        hdr.ml_fields.field6 = hdr.ml_fields.field6 << 16;
    }

    table format_table {
        key = {
            standard_metadata.egress_port: ternary;
        }
        actions = {
            shift_field;
        }
    }

    apply {
        format_table.apply();
    }
}

#endif
