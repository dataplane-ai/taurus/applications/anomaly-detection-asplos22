/*
 * Copyright 2021 Tushar Swamy (Stanford University),
 *                Alexander Rucker (Stanford University),
 *                Annus Zulfiqar (Purdue University),
 *                Muhammad Shahbaz (Stanford/Purdue University)
 *
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __FORMAT_FXPT__
#define __FORMAT_FXPT__

#include "headers.p4"
#include "defines.p4"

control drop_anomaly_control(inout parsed_headers_t hdr,
                        inout standard_metadata_t standard_metadata) {


    action install_rule() {
	// Trigger rule install
    }

 
    action drop_anomaly() {
    	hdr.ml_fields.prediction = hdr.ml_fields.prediction | 1
    }


    table install_rule_table {
        key = {
	    hdr.ml_fields.prediction: exact;
        }
        actions = {
            install_rule;
        }
    }

    table drop_anomaly_table {
        key = {
	    local_metadata.flow_idx: exact;
        }
        actions = {
            drop_anomaly;
        }
    }

    apply {
        drop_anomaly_table.apply();
	install_rule_table.apply();
     }
}

#endif
