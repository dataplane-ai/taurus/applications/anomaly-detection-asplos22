/*
 * Copyright 2021 Tushar Swamy (Stanford University),
 *                Alexander Rucker (Stanford University),
 *                Annus Zulfiqar (Purdue University),
 *                Muhammad Shahbaz (Stanford/Purdue University)
 *
 * Copyright 2017-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __ACCUMULATE__
#define __ACCUMULATE__

#include "headers.p4"
#include "defines.p4"

control accumulate_control(inout parsed_headers_t hdr,
			inout local_metadata_t local_metadata,
                        inout standard_metadata_t standard_metadata) {


    register<bit<32>>(128) field0_regfile;
    register<bit<32>>(128) field1_regfile;
    register<bit<32>>(128) field2_regfile;
    register<bit<32>>(128) field3_regfile;
    register<bit<32>>(128) field4_regfile;
    register<bit<32>>(128) field5_regfile;
    register<bit<32>>(128) field6_regfile;


    action accumulate_field() {
	@atomic {

	    local_metadata.field0_tmp = 0;
	
            field0_regfile.read(local_metadata.field0_tmp, local_metadata.index);
	    hdr.ml_fields.field0 = hdr.ml_fields.field0 + local_metadata.field0_tmp;
	    field0_regfile.write(local_metadata.index, hdr.ml_fields.field0);
	    
	    local_metadata.field1_tmp = 0;
            field1_regfile.read(local_metadata.field1_tmp, local_metadata.index);
	    hdr.ml_fields.field1 = hdr.ml_fields.field1 + local_metadata.field1_tmp;
	    field1_regfile.write(local_metadata.index, hdr.ml_fields.field1);

	    local_metadata.field2_tmp = 0;
            field2_regfile.read(local_metadata.field2_tmp, local_metadata.index);
	    hdr.ml_fields.field2 = hdr.ml_fields.field2 + local_metadata.field2_tmp;
	    field2_regfile.write(local_metadata.index, hdr.ml_fields.field2);
	    
	    local_metadata.field3_tmp = 0;
            field3_regfile.read(local_metadata.field3_tmp, local_metadata.index);
	    hdr.ml_fields.field3 = hdr.ml_fields.field3 + local_metadata.field3_tmp;
	    field3_regfile.write(local_metadata.index, hdr.ml_fields.field3);

	    local_metadata.field4_tmp = 0;
            field4_regfile.read(local_metadata.field4_tmp, local_metadata.index);
	    hdr.ml_fields.field4 = hdr.ml_fields.field4 + local_metadata.field4_tmp;
	    field4_regfile.write(local_metadata.index, hdr.ml_fields.field4);

	    local_metadata.field5_tmp = 0;
            field5_regfile.read(local_metadata.field5_tmp, local_metadata.index);
	    hdr.ml_fields.field5 = hdr.ml_fields.field5 + local_metadata.field5_tmp;
	    field5_regfile.write(local_metadata.index, hdr.ml_fields.field5);
	
	    local_metadata.field6_tmp = 0;
            field6_regfile.read(local_metadata.field6_tmp, local_metadata.index);
	    hdr.ml_fields.field6 = hdr.ml_fields.field6 + local_metadata.field6_tmp;
	    field6_regfile.write(local_metadata.index, hdr.ml_fields.field6);
    	}
    }

    table accumulate_table {
        key = {
            standard_metadata.ingress_port: ternary;
        }
        actions = {
            accumulate_field;
        }
    }

    apply {
        accumulate_table.apply();
     }
}

#endif
